output "stage_dns_name" {
  value = aws_lb.docker_stage.dns_name
}

output "stage_zone_id" {
  value = aws_lb.docker_stage.zone_id
}

output "stage_target_gp_arn" {
  value = aws_lb_target_group.docker_stage_target_group.arn
}