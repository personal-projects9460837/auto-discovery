#!/bin/bash
sudo hostnamectl set-hostname ansible
sudo apt update
sudo apt-add-repository ppa:ansible/ansible
sudo apt update -y
sudo apt install ansible -y
sudo apt install python3-pip -y
sudo apt install python3-boto3 -y

# Copying private key into ansible Server
echo "${priv_key}" >> /home/ubuntu/.ssh/myKey
sudo chown -R ubuntu:ubuntu /home/ubuntu/.ssh/myKey
sudo chmod 400 /home/ubuntu/.ssh/myKey

#Creating ansible variables files
touch /home/ubuntu/doc.yaml
sudo echo docker_password: (myDockerhubpassword) >> /home/ubuntu/doc.yaml
sudo chown -R ubuntu:ubuntu /home/ubuntu/doc.yaml
sudo chmod 700 /home/ubuntu/doc.yaml


#Creating hosts file
touch /home/ubuntu/hosts
sudo chown -R ubuntu:ubuntu /home/ubuntu/hosts

