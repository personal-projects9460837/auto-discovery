variable "ami_ubuntu" {}
variable "key_pair" {}
variable "priv_subnet1" {}
variable "ansible_sg" {}
variable "priv_key" {}
variable "instance_profile" {}
variable "bastion-host" {}
