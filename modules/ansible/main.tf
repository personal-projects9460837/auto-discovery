# launch the ec2 instance and install website
resource "aws_instance" "ansible_server" {
  ami                    = var.ami_ubuntu
  instance_type          = "t2.micro"
  subnet_id              = var.priv_subnet1
  iam_instance_profile   = var.instance_profile
  vpc_security_group_ids = [var.ansible_sg]
  key_name               = var.key_pair
  user_data              = templatefile("./modules/ansible/ansible.sh", {
    priv_key = var.priv_key
  })

  tags = {
    Name = "ansible-webserver"
  }
}

# create null resource to copy playbook directory into ansible server
resource "null_resource" "copy-playbook" {
  connection {
    type = "ssh"
    host = aws_instance.ansible_server.private_ip
    user = "ubuntu"
    private_key = var.priv_key
    bastion_host = var.bastion-host
    bastion_user = "ubuntu"
    bastion_host_key = var.priv_key

  }
  provisioner "file" {
    source = "./modules/ansible/playbooks"
    destination = "/home/ubuntu/playbooks"
  }

  provisioner "file" {
    source      = "./ansible.cfg"
    destination = "/home/ubuntu/ansible.cfg"
  }

  provisioner "file" {
    source      = "./aws_ec2.yaml"
    destination = "/home/ubuntu/aws_ec2.yaml"
  }

  provisioner "file" {
    source      = "./Dockerfile"
    destination = "/home/ubuntu/Dockerfile"
  }

  provisioner "file" {
    source      = "./proj.sh"
    destination = "/home/ubuntu/proj.sh"
  }

  provisioner "remote-exec" {
    inline = [ 
      "sudo chmod +x /home/ubuntu/proj.sh"
      ]
  }

}