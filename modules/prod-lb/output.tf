output "prod_dns_name" {
  value = aws_lb.docker_prod.dns_name
}

output "prod_zone_id" {
  value = aws_lb.docker_prod.zone_id
}

output "prod_target_gp_arn" {
  value = aws_lb_target_group.docker_prod_target_group.arn
}