# create application load balancer for prod
resource "aws_lb" "docker_prod" {
  name               = "docker-prod-alb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [var.lb_sg]
  subnets            = [var.pub_subnet_1, var.pub_subnet_2]
  enable_deletion_protection = false

  tags   = {
    Name = "docker-prod-alb"
  }
}

# create target group for prod
resource "aws_lb_target_group" "docker_prod_target_group" {
  name        = "docker-prod-tg"
  target_type = "instance"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = var.vpc_id

  health_check {
    enabled             = true
    interval            = 30
    path                = "/"
    matcher             = "200" 
    timeout             = 5
    healthy_threshold   = 5
    unhealthy_threshold = 5
  }

  lifecycle {
    create_before_destroy = true
  }
}

# create a listener on port 80 with redirect action
resource "aws_lb_listener" "prod_http_listener" {
  load_balancer_arn = aws_lb.docker_prod.arn
  port              = 80
  protocol          = "HTTP"

  default_action {
    type = "redirect"

    redirect {
      port        = 443
      protocol    = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}

# create a listener on port 443 with forward action
resource "aws_lb_listener" "prod_https_listener" {
  load_balancer_arn  = aws_lb.docker_prod.arn
  port               = 443
  protocol           = "HTTPS"
  ssl_policy         = "ELBSecurityPolicy-2016-08"
  certificate_arn    = var.certificate_arn

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.docker_prod_target_group.arn
  }
}