output "ami_ubuntu" {
  value = data.aws_ami.ubuntu.id
}

output "pub_key" {
  value = aws_key_pair.kp.key_name
}

output "priv_key" {
  value = tls_private_key.pk.private_key_pem
}