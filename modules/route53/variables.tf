variable "domain_name" {}
variable "domain_name2" {}

variable "jenkins_record_name" {}
variable "jenkins_lb_dns_name" {}
variable "jenkins_lb_zone_id" {}

variable "prod_record_name" {}
variable "prod_lb_dns_name" {}
variable "prod_lb_zone_id" {}

variable "stage_record_name" {}
variable "stage-lb-dns-name" {}
variable "stage-lb-zone-id" {}
