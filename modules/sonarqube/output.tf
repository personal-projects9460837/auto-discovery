output "sonar_pub_ip" {
  value = aws_instance.sonarqube-server.public_ip
}