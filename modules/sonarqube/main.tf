resource "aws_instance" "sonarqube-server" {
  ami                    = var.ami_ubuntu
  instance_type          = "t2.medium"
  subnet_id              = var.pub-sub-1
  vpc_security_group_ids = [var.sonar_sg]
  key_name               = var.key_pair
  user_data              = local.sonarqube_user_data

  tags = {
    Name = "sonarqube-webserver"
  }
}