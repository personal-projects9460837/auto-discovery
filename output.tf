output "bastion_public_ip" {
  value = module.bastion_server.bastion_ip
}

output "jenkins-servers-ip" {
  value = module.jenkins_server.jenkins_ip
}

output "jenkins-servers-dns" {
  value = module.jenkins_server_lb.jenkins_dns_name
}

output "ansible-servers-ip" {
  value = module.ansible_server.ansible_ip
}
output "sonar-ip" {
  value = module.sonar.sonar_pub_ip
}

output "stage_lb_dns_name" {
  value = module.stage_lb.stage_dns_name
}

output "prod_lb_dns_name" {
  value = module.prod_lb.prod_dns_name
}

output "smtp_username" {
  value = module.smtp_email.smtp_username
}

output "smtp_password" {
  value = module.smtp_email.smtp_password
  sensitive = true
}
